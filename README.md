# SAR service

This is just a data service collector for the HTML based Arduino app to collect some time based data.

SAR is just a shortcut for Sensor Array R (where R was something very important and I totally forgot that)

## Example of SAR

It is in `sar_unit_folder`, basically it's just anything able to make a response on port `9010` and send back following example as an request:

```
~$ echo "" | nc 10.0.0.172 9010 
HTTP/1.1 200 OK
Content-Type: text/html
Connection: close
Refresh: 5

<!DOCTYPE HTML>
<html>
<pre>
Temperature: 33.10 C
Humidity: 9.40 %
Smoke: 3.1
Raw smoke: 638
CO2: 148.05
</pre></html>
```

Those `http` commands are there just for browser to refresh the page in five seconds. It looks nice and everyone could be happy about it.


## Example of service

First of all you need to define a list of `SAR` devices, that could be done in `sar_list.txt`. Obviously, there are three columns, first is the name of `SAR` device, second is its IP address and third is the attribute collected from the device. E.g:

```
sar_stod 10.0.1.172 Humidity
sar_stod 10.0.1.172 Temperature
sar_modr 10.0.0.172 Temperature
sar_modr 10.0.0.172 Humidity
sar_modr 10.0.0.172 Smoke
sar_modr 10.0.0.172 CO2
```

After that you can try run `bash service.sh`, if your SAR devices are available, you'll get `RRD` database in `databases` folder and after some time, you will get graphs in `graphs`.

If you want to use this as `systemd` service, edit the `sar.service` to the correct folder and let it run.