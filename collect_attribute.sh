#!/bin/bash

[[ $1 == "" || $2 == "" || $3 == "" ]] && {
  echo "./$0 <sar_name> <sar_ip/hostname> <attribute>";
  exit 1;
}


echo "" | nc $2 9010 | grep $3 | awk '{$1=""; print $0;}' |  grep -o '[0-9\.]\+';
