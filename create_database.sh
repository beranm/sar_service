#!/bin/bash

[[ $1 == "" || $2 == "" ]] && {
  echo "./$0 <name of sar> <attribute>";
  exit 1;
}

[[ -f "./databases/$1_$2.rrd" ]] && {
  echo "Database already created, leaving!";
  exit 1;
}


rrdtool create "./databases/$1_$2.rrd" \
  --start now --step 1s \
  DS:$2:GAUGE:5m:-100:100 \
  RRA:AVERAGE:0.5:1s:10d \
  RRA:AVERAGE:0.5:1m:90d \
  RRA:AVERAGE:0.5:1h:18M \
  RRA:AVERAGE:0.5:1d:60y;
