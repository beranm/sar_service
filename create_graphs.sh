#!/bin/bash


function graph(){
  first=$1;
  second=$2;
  database=$3;
  attribute=$4;
  sar_name=$5;
  interval=$6;
 
  /usr/bin/rrdtool graph - \
    --imgformat=PNG \
    --start=${first} \
    --end=${second} \
    --title="$sar_name $attribute" \
    --rigid \
    --base='1000' \
    --height='300' \
    --width='900' \
    --alt-autoscale-max \
    --lower-limit='-20' \
    --vertical-label='' \
    --slope-mode \
    --font TITLE:10: \
    --font AXIS:7: \
    --font LEGEND:8: \
    --font UNIT:7: \
    DEF:a=${database}:${attribute}:AVERAGE \
    LINE1:a#FF0000FF:''  \
    GPRINT:a:LAST:'Last\:%8.0lf'  \
    GPRINT:a:AVERAGE:'Average\:%8.0lf'  \
    GPRINT:a:MAX:'Max\:%8.0lf\n' \
    GPRINT:a:MIN:'Min\:%8.0lf\n' > ./graphs/${sar_name}_${attribute}_${interval}.png;
}

[[ $1 == "" || $2 == "" ]] && {
  echo "./$0 <name of sar> <attribute>";
  exit 1;
}


# plot a day
first=`date +%s -d "1 day ago"`;
second=`date +%s`;
graph $first $second "./databases/$1_$2.rrd" $2 $1 "day";

# plot a week
first=`date +%s -d "7 day ago"`;
second=`date +%s`;
graph $first $second "./databases/$1_$2.rrd" $2 $1 "week";

# plot a month
first=`date +%s -d "30 day ago"`;
second=`date +%s`;
graph $first $second "./databases/$1_$2.rrd" $2 $1 "month";

# plot a year
first=`date +%s -d "365 day ago"`;
second=`date +%s`;
graph $first $second "./databases/$1_$2.rrd" $2 $1 "year";

