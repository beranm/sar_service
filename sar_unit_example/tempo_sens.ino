#include <UIPEthernet.h>
#include "DHT.h"

#define DHTPIN 4
#define DHTTYPE DHT21

byte mac[] = { 0x90, 0xA2, 0xDA, 0x0D, 0x78, 0xEE  };  
IPAddress ip(10, 0, 1, 172);
static byte gateway[] = { 10, 0, 1, 138 };
static byte subnet[] = { 255, 255, 255, 0};

EthernetServer server(9010);

DHT dht(DHTPIN, DHTTYPE);

void setup() {
 Serial.begin(9600); 
 Serial.println("DHTxx test!");
 dht.begin();

 Ethernet.begin(mac, ip, gateway, subnet);
 server.begin();
 Serial.print("server is at ");
 Serial.println(Ethernet.localIP());
}


void loop() {
  EthernetClient client = server.available();
  float h;
  float t;
  if (client) {
    Serial.println("new client");
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        if (c == '\n' && currentLineIsBlank) {
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");
          client.println("Refresh: 5");
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          client.println("<pre>");
          h = dht.readHumidity();
          t = dht.readTemperature();
          if (isnan(t) || isnan(h)) {
            client.println("Failed to read from DHT");
          } else {
            client.print("Humidity: "); 
            client.print(h);
            client.print(" %\n");
            client.print("Temperature: "); 
            client.print(t);
            client.println(" C\n");
          }          
          client.println("</pre></html>");
          break;
        }
        if (c == '\n') {
          currentLineIsBlank = true;
        } else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    delay(1);
    client.stop();
    Serial.println("client disconnected");
  }
  delay(10);
}


