#!/bin/bash

n=0;


while [[ 1 ]];do
  
  while read line;do
    sar_name=`echo $line | awk '{print $1}'`;
    sar_ip=`echo $line | awk '{print $2}'`;
    sar_attribute=`echo $line | awk '{print $3}'`;
    
    bash create_database.sh $sar_name $sar_attribute >/dev/null;
  
    collected_data=`bash ./collect_attribute.sh $sar_name $sar_ip $sar_attribute`;
    rrdtool update ./databases/${sar_name}_${sar_attribute}.rrd N:${collected_data};
  done < sar_list.txt;
  
  n=$(($n + 1));
  sleep 1;


  if [[ $(($n%60)) == 0 ]];then
    echo "Creating graphs";
    while read line;do
      sar_name=`echo $line | awk '{print $1}'`;
      sar_ip=`echo $line | awk '{print $2}'`;
      sar_attribute=`echo $line | awk '{print $3}'`;
      
      bash ./create_graphs.sh $sar_name $sar_attribute &
    done < sar_list.txt;
  fi


  echo "Iteration ${n}";
done;
